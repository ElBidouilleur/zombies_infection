add_rules("mode.debug", "mode.releasedbg", "mode.release")

add_repositories("nazara-engine-repo https://github.com/NazaraEngine/xmake-repo")

add_requires("nazaraengine", { configs = { debug = is_mode('debug') }})

add_rules("plugin.vsxmake.autoupdate")

set_project("Zombies_Infection")
set_version("0.0.1")

set_languages("c++20")

set_rundir(".")

if is_plat('windows') then
    set_runtimes(is_mode("debug") and "MDd" or "MD")
end

set_warnings("allextra")


target("ZI", function()

    set_kind("binary")

    add_files("src/**.cpp")
    add_headerfiles("src/**.hpp")

    add_packages("nazaraengine", {components = "graphics", "physics2d", "audio"})

end)