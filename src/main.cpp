#include <iostream>

#include <Nazara/Core.hpp>
#include <Nazara/Graphics.hpp>
#include <Nazara/Platform.hpp>
#include <Nazara/Utility.hpp>
#include <Nazara/Renderer.hpp>

int main(int argc, char* argv[])
{
	Nz::Application<Nz::Graphics> app(argc, argv);

	auto& fileSystemComponent = app.AddComponent<Nz::AppFilesystemComponent>();
	fileSystemComponent.Mount("assets", Nz::Utf8Path("../Assets"));

	auto& windowComponent = app.AddComponent<Nz::AppWindowingComponent>();
	auto& window = windowComponent.CreateWindow(Nz::VideoMode(1280, 720), "Zombies Infection");

	auto& ecsComponent = app.AddComponent<Nz::AppEntitySystemComponent>();

	auto& enttWorld = ecsComponent.AddWorld<Nz::EnttWorld>();

	auto& renderSystem = enttWorld.AddSystem<Nz::RenderSystem>();
	auto& windowSwapchain = renderSystem.CreateSwapchain(window);
	

	entt::handle camera = enttWorld.CreateEntity();
	{
		camera.emplace<Nz::NodeComponent>();
		auto& cameraComponent = camera.emplace<Nz::CameraComponent>(&windowSwapchain, Nz::ProjectionType::Orthographic);

		cameraComponent.UpdateClearColor(Nz::Color::Blue());
	}

	entt::handle playerEntity = enttWorld.CreateEntity();
	{
		playerEntity.emplace<Nz::NodeComponent>();

		Nz::TextureSamplerInfo playerSampler;
		playerSampler.anisotropyLevel = 16;

		std::shared_ptr<Nz::MaterialInstance> playerMaterial = Nz::Graphics::Instance()->GetDefaultMaterials().basicMaterial->Instantiate();
		playerMaterial->SetTextureProperty("BaseColorMap", fileSystemComponent.Load<Nz::Texture>("assets/graphics/player.png"), playerSampler);

		std::shared_ptr<Nz::Sprite> sprite = std::make_shared<Nz::Sprite>(playerMaterial);
		sprite->SetSize(Nz::Vector2f(313, 206));
		sprite->SetColor(Nz::Color::White());
		sprite->SetTextureRect(Nz::Rectf(0,0,313,206));

		playerEntity.emplace<Nz::GraphicsComponent>(sprite);
	}

	return app.Run();
}
